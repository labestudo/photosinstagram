"use strict"
const puppeteer = require ('puppeteer');
const fs = require ('fs');

(async () => {
    const url = ("https://www.instagram.com/pensandoalto8/")
    // para trocar a página, altere o valorde 'pageWeb'
    const pageWeb = (`${url}`);
    const browser = await puppeteer.launch({ headless: false});
    const page = await browser.newPage();
    await page.goto(pageWeb);

    
    const imgList = await page.evaluate(() => {
        // toda essa função vai ser executada no browser

        // pegar imagens que estão postadas
        const nodeList = document.querySelectorAll('article img')
        // transformar o NodeList em array
        const imgArray = [...nodeList]
        //transformar os nodes rm objetos javascript
        const imgList = imgArray.map( ({src}) => ({
            src
        }))
        console.log(imgList)
        
        return imgList
    });
    
    // escrever os dados em um arquivo json
    fs.writeFile('instagram.json', JSON.stringify(imgList, null, 3), err =>{
        if (err) throw new Error ('Something went wrong')
        
        console.log('Well done')
    })

   await browser.close();
})();